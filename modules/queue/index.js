const core = require('node-core')
const events = require('events')
const networking = require('node-network')
const enums = require('./enums')
const ItemManager = require('./item-manager')
const models = require('./models')
const eventListenerManager = require('./event-listener-manager')

const logger = core.logger

const Queue = function () {
  const _this = this
  let _itemManager

  const _server = new networking.SocketServer({ host: process.network.myAddress }, {
    register: (req, res) => {
      try {
        switch (req.data.type) {
          case enums.CLIENT_TYPES.QUEUE_ITEM_HANDLER:
            models.QueueItemHandler.call(req.__socket, req.data)
            _itemManager.emit('queue-item-handler-connected', req.__socket)
            break
          case enums.CLIENT_TYPES.QUEUE_ITEM_RECEIVER:
            models.QueueItemReceiver.call(req.__socket, req.data)
            _itemManager.emit('queue-item-receiver-connected', req.__socket)
            break
          case enums.CLIENT_TYPES.EVENT_LISTENER:
            models.EventListener.call(req.__socket, req.data)
            eventListenerManager.emit('event-listener-connected', req.__socket)
            break
          default:
            throw new Error('Invalid \'type\' specified')
        }
      } catch (e) {
        return res.sendError(e, () => {
          req.__socket.destroy()
        })
      }

      res.send()
    }
  }, [], _this)

  _server.on('listening', () => {
    logger.info(`tcp://${_server.address().address}:${_server.address().port} listening`)
    _this.emit('ready', _server.address())
  })

  _itemManager = new ItemManager()
  _itemManager.on('ready', () => {
    _server.listen()
  })

  _this.readyToTerminate = _itemManager.readyToTerminate
  _this.prepareForTerminate = () => {
    if (_server) {
      _server.close()
    }
  }

  return _this
}

Queue.prototype = events.EventEmitter.prototype
module.exports = () => {
  module.exports = new Queue()
  return module.exports
}
