const core = require('node-core')

const logger = core.logger

// this 'class' is intended to be invoked with the socket:
// eg: EventListener.call(socket, data);
// by doing this, we wrap the socket with properties specfic
// to how we intend to use this socket

const EventListener = function (data) {
  const _this = this

  _this.events = data.events

  logger.debug('Event listener client registered')
  return _this
}

module.exports = EventListener
