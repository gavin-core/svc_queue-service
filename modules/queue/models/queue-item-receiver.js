// this 'class' is intended to be invoked with the socket:
// eg: QueueItemReceiver.call(socket, data);
// by doing this, we wrap the socket with properties specfic
// to how we intend to use this socket

const QueueItemReceiver = function (data) {
  const _this = this

  _this.registerHPCs({
    newItem: (req, res) => {
      const id = req.data

      res.send(null, () => {
        _this.emit('new-item', id)
      })
    }
  })

  return _this
}

module.exports = QueueItemReceiver
