const core = require('node-core')

const logger = core.logger

// this 'class' is intended to be invoked with the socket:
// eg: QueueItemHandler.call(socket, data);
// by doing this, we wrap the socket with properties specfic
// to how we intend to use this socket

const QueueItemHandler = function (data) {
  const _this = this

  if (!+data.handlerCount) {
    throw new Error('No \'handlerCount\' was specified, ignoring this handler')
  }

  if (!data.tasks || !data.tasks.length) {
    throw new Error('No \'tasks\' were specified, ignoring this handler')
  }

  _this.handlerCount = data.handlerCount
  _this.tasks = data.tasks
  _this.currentItems = []

  _this.maxLoadReached = () => _this.handlerCount <= _this.currentItems.length

  _this.queueItem = item => {
    for (let i = 0; i < _this.currentItems.length; i++) {
      if (_this.currentItems[i]._id.toString() === item._id.toString()) {
        logger.debug('Item has already been queued on this handler - Possible error')
        return
      }
    }

    _this.currentItems.push(item)
  }

  _this.processItem = (item, cb) => {
    cb = cb || (() => {})

    if (typeof item === 'string') {
      for (let i = 0; i < _this.currentItems.length; i++) {
        if (_this.currentItems[i]._id.toString() === item) {
          item = _this.currentItems[i]
          break
        }
      }
    } else {
      let found = false
      for (let i = 0; i < _this.currentItems.length; i++) {
        if (_this.currentItems[i]._id.toString() === item._id.toString()) {
          found = true
          break
        }
      }

      if (!found) {
        item = null
      }
    }

    if (!item) {
      return cb(new Error('You have to queue the item before processing'))
    }

    const task = item.getCurrentTask()
    const data = item.getCurrentTaskData()

    data.__id = item._id

    _this.rpcs.handle({ id: item._id, task: task.id, version: task.version, data: data }, err => {
      if (!err) {
        logger.info(`Assigned task: ${task.id} on item ${item._id}`)
        return cb()
      }

      for (let i = _this.currentItems.length - 1; i >= 0; i--) {
        if (_this.currentItems[i]._id.toString() === item._id.toString()) {
          _this.currentItems.splice(i, 1)
        }
      }

      cb(err)
    })
  }

  _this.registerHPCs({
    deregister: (req, res) => {
      // TODO - allow a handler to deregister one or more task handlers (currently we force a complete deregistration)

      _this.tasks = []
      _this.emit('deregister')
      res.send()
    },
    handled: (req, res) => {
      const id = req.data.id
      const data = req.data.data

      logger.capture(`Item ${id} handled`)

      for (let i = _this.currentItems.length - 1; i >= 0; i--) {
        if (_this.currentItems[i]._id.toString() === id.toString()) {
          _this.currentItems.splice(i, 1)
        }
      }

      res.send(null, () => {
        _this.emit('task-handled', id, data)
      })
    },
    failed: (req, res) => {
      const id = req.data.id
      const err = req.data.err

      logger.warning(`Item ${id} failed`)

      for (let i = _this.currentItems.length - 1; i >= 0; i--) {
        if (_this.currentItems[i]._id.toString() === id.toString()) {
          _this.currentItems.splice(i, 1)
        }
      }

      res.send(null, () => {
        _this.emit('task-failed', id, err)
      })
    },
    delete: (req, res) => {
      const id = req.data.id
      const err = req.data.err

      logger.warning(`Item ${id} returned as a delete`)

      for (let i = _this.currentItems.length - 1; i >= 0; i--) {
        if (_this.currentItems[i]._id.toString() === id.toString()) {
          _this.currentItems.splice(i, 1)
        }
      }

      res.send(null, () => {
        _this.emit('task-delete', id, err)
      })
    }
  })

  _this.registerRPCs(['HANDLE'])

  return _this
}

module.exports = QueueItemHandler
