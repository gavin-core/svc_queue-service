const core = require('node-core')
const QueueTask = require('./queue-task')

const _ = core._

const QueueItem = function (item) {
  const _this = _.extend(this, item)

  for (let i = 0; i < _this.tasks.length; i++) {
    _this.tasks.splice(i, 1, new QueueTask(_this.tasks[i], i))
  }

  _this.data = _this.data || {}
  _this.taskData = _this.taskData || {}

  return _this
}

QueueItem.prototype.isAssigned = function () {
  return !!this.assigned
}

QueueItem.prototype.isAvailable = function () {
  return !this.durationTilNextAvailable()
}

QueueItem.prototype.getCurrentTask = function () {
  for (let i = 0; i < this.tasks.length; i++) {
    if (!this.tasks[i].complete && this.tasks[i].dateStarted) {
      return this.tasks[i]
    }
  }

  return null
}

QueueItem.prototype.getCurrentTaskData = function () {
  const _this = this
  const currentTask = this.getCurrentTask()

  if (!currentTask) {
    throw new Error('No current task to get data for')
  }

  let data = _.extendOwn({}, currentTask.data)

  // if (this.taskData[index] && this.taskData[index].out)
  // return this.taskData[index].out

  const checkString = value => {
    const holder = value

    if (value.indexOf('{{') < 0 || value.indexOf('}}') <= 0) {
      return value
    }

    const startIndex = value.indexOf('{{') + 2
    const endIndex = value.indexOf('}}')
    const notatedValue = value.substring(startIndex, endIndex)

    const parts = _.filter(notatedValue.split(/\.|\[|\]/g), val => val !== '')

    if (!parts || !parts.length) {
      return null
    }

    if (parts[0] === 'data') {
      value = _this.data
    } else {
      value = _this.taskData[parts[0]]
    }

    parts.shift()
    for (let j = 0; j < parts.length && value != null; j++) {
      value = value[parts[j]]
    }

    if (typeof value === 'string') {
      value = holder.substring(0, startIndex - 2) + value + holder.substring(endIndex + 2)
      return checkString(value)
    }

    return value
  }

  const checkObject = data => {
    const keys = _.keys(data)

    for (let i = 0; i < keys.length; i++) {
      const key = keys[i]
      let value = data[key]

      if (Array.isArray(value)) {
        data[key] = value = checkArray(value)
      } else if (typeof value === 'string') {
        data[key] = value = checkString(value)
      } else {
        data[key] = value = checkObject(value)
      }
    }

    return data
  }

  const checkArray = data => {
    for (let i = 0; i < data.length; i++) {
      data[i] = check(data[i])
    }

    return data
  }

  const check = data => {
    if (typeof data === 'string') {
      return checkString(data)
    }

    if (Array.isArray(data)) {
      return checkArray(data)
    }

    return checkObject(data)
  }

  data = check(data)
  return data
}

QueueItem.prototype.setCurrentTaskData = function (data) {
  var currentTask = this.getCurrentTask()
  if (!currentTask) {
    throw new Error('No current task to set data for')
  }

  this.taskData[currentTask.index]['in'] = data
}

QueueItem.prototype.currentTaskCompleted = function () {
  this.getCurrentTask().completed()
  this.assigned = false

  if (this.isComplete()) {
    this.success = true
    return
  }

  const nextTask = this.getNextTask()
  if (!+nextTask.delay) {
    this.nextAvailable = null
  } else {
    this.nextAvailable = +new Date() + nextTask.delay
  }
}

QueueItem.prototype.currentTaskFailed = function (err, nextAvailable) {
  const task = this.getCurrentTask()
  if (!task) {
    return
  }

  this.taskData[task.index].err = this.taskData[task.index].err || { index: -1, data: null }
  this.taskData[task.index].err.index++
  this.taskData[task.index].err.data = err

  this.getCurrentTask().failed()

  const escalation = 2000 * this.taskData[task.index].err.index
  this.nextAvailable = +new Date() + (+nextAvailable || task.failedTimeout || escalation)
  this.assigned = false
}

QueueItem.prototype.currentTaskRejected = function () {
  var task = this.getCurrentTask()
  if (!task) {
    return
  }

  this.getCurrentTask().rejected()
  this.nextAvailable = +new Date() + 5000
  this.assigned = false
}

QueueItem.prototype.getNextTask = function () {
  for (let i = 0; i < this.tasks.length; i++) {
    if (!this.tasks[i].complete && !this.tasks[i].dateStarted) {
      return this.tasks[i]
    }
  }

  return null
}

QueueItem.prototype.isComplete = function () {
  return _.all(this.tasks, { complete: true })
}

QueueItem.prototype.durationTilNextAvailable = function () {
  if (!+this.nextAvailable) {
    return 0
  }

  const duration = +this.nextAvailable - +new Date()
  if (duration > 0) {
    return duration
  }

  return 0
}

QueueItem.prototype.beginNextTask = function () {
  const nextTask = this.getNextTask()
  if (!nextTask) {
    throw new Error('No next task available')
  }

  nextTask.begin()
  this.taskData[nextTask.index] = this.taskData[nextTask.index] || {
    out: null,
    'in': null
  }

  this.taskData[nextTask.index].out = this.getCurrentTaskData()

  return nextTask
}

QueueItem.prototype.isFirstTask = function (task) {
  return task === this.tasks[0]
}

module.exports = QueueItem
