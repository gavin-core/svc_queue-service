const core = require('node-core')

const _ = core._

const QueueTask = function (task, index) {
  const _this = _.extend(this, task)

  _this.index = _this.index || index

  return _this
}

QueueTask.prototype.begin = function () {
  this.dateStarted = +new Date()
}

QueueTask.prototype.failed = function () {
  this.dateStarted = null
  this.complete = false
}

QueueTask.prototype.rejected = function () {
  this.dateStarted = null
  this.complete = false
}

QueueTask.prototype.completed = function () {
  this.dateCompleted = +new Date()
  this.complete = true
}

module.exports = QueueTask
