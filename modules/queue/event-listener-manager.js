const events = require('events')

const EventListenerManager = function () {
  const _this = this
  const _listeners = []
  const _eventListeners = {}

  _this.on('event-listener-connected', listener => {
    _listeners.push(listener)

    for (let i = 0; i < listener.events.length; i++) {
      if (!(listener.events[i] in _eventListeners)) {
        _eventListeners[listener.events[i]] = []
      }
      _eventListeners[listener.events[i]].push(listener)
    }

    listener.on('disconnect', () => {
      for (let i = 0; i < this.events.length; i++) {
        for (let j = _eventListeners[this.events[i]].length; j >= 0; j--) {
          if (_eventListeners[this.events[i]][j] === this) {
            _eventListeners[this.events[i]].splice(j, 1)
            break
          }
        }
      }

      for (let i = _listeners.length - 1; i >= 0; i--) {
        if (_listeners[i] === this) {
          _listeners.splice(i, 1)
          break
        }
      }
    })

    listener.registerHPCs({
      claim: (req, res) => {
        // TODO - an event listener should be able to claim the event,
        // TODO - in which case, it signals it will handle the event, preventing
        // TODO - another similar service from attmepting to handle it
        res.send(true)
      }
    })
  })

  _this.broadcast = (id, item, err) => {
    if (!_eventListeners[id] || !_eventListeners[id].length) {
      return // console.log('No listeners to broadcast to: ' + id)
    }

    for (let i = 0; i < _eventListeners[id].length; i++) {
      _eventListeners[id][i].sendJson(id, { item: item, err: err })
    }
  }

  return _this
}

EventListenerManager.prototype = events.EventEmitter.prototype
module.exports = new EventListenerManager()
