const core = require('node-core')
const events = require('events')
const db = require('../../middleware/db')
const models = require('./models')
const eventListenerManager = require('./event-listener-manager')

const logger = core.logger
const _ = core._

const ItemManager = function () {
  const _this = this
  const _items = []
  const _handlers = []
  const _taskHandlers = {}

  _this.on('queue-item-handler-connected', handler => {
    if (process.askedToTerminate) {
      return
    }

    logger.debug('Queue Item Handler connected and registered')

    _handlers.push(handler)

    for (let i = 0; i < handler.tasks.length; i++) {
      if (!(handler.tasks[i] in _taskHandlers)) {
        _taskHandlers[handler.tasks[i]] = []
      }

      _taskHandlers[handler.tasks[i]].push(handler)
    }

    handler.on('deregister', () => {
      const taskKeys = _.keys(_taskHandlers)

      for (let i = 0; i < taskKeys.length; i++) {
        for (let j = _taskHandlers[taskKeys[i]].length - 1; j >= 0; j--) {
          if (_taskHandlers[taskKeys[i]][j] === handler) {
            _taskHandlers[taskKeys[i]].splice(j, 1)
          }
        }
      }

      logger.debug('Queue Item Handler client deregistered')
    })

    handler.on('disconnect', () => {
      const taskKeys = _.keys(_taskHandlers)

      for (let i = 0; i < taskKeys.length; i++) {
        for (let j = _taskHandlers[taskKeys[i]].length - 1; j >= 0; j--) {
          if (_taskHandlers[taskKeys[i]][j] === handler) {
            _taskHandlers[taskKeys[i]].splice(j, 1)
          }
        }
      }

      for (let i = handler.currentItems.length - 1; i >= 0; i--) {
        handler.currentItems[i].currentTaskFailed()
        db.getDb().queue_items.save(handler.currentItems[i])
        _queueItem(handler.currentItems[i])
        handler.currentItems.splice(i, 1)
      }

      for (let i = 0; i < _handlers.length; i++) {
        if (handler === _handlers[i]) {
          _handlers.splice(i, 1)
        }
      }

      logger.debug('Queue Item Handler client disconnected')
    })

    handler.on('task-handled', (id, data) => {
      const item = _.find(_items, item => item._id.toString() === id)
      let task

      item.setCurrentTaskData(data)
      task = item.getCurrentTask()
      item.currentTaskCompleted()

      if (item.isComplete()) {
        const index = _items.findIndex(x => x._id === item._id)
        if (index >= 0) {
          _items.splice(index, 1)
        } else {
          throw new Error('OMG!!!')
        }

        return db.getDb().queue_items.remove({ _id: id.toObjectID() }, () => {
          db.getDb().queue_item_history.save(item, () => {
            eventListenerManager.broadcast(`Completed_${item.operation}_${task.id}`, item)
            eventListenerManager.broadcast(`Completed_${task.id}`, task)
            eventListenerManager.broadcast(`Completed_${item.operation}_Task`, item)
            eventListenerManager.broadcast(`Completed_${item.operation}`, item)

            process.nextTick(() => {
              _queueItemToHandler(handler)
            })
          })
        })
      }

      db.getDb().queue_items.save(item, () => {
        eventListenerManager.broadcast(`Completed_${task.id}`, task)
        eventListenerManager.broadcast(`Completed_${item.operation}_Task`, item)
        process.nextTick(() => {
          _queueItem(item)
          _queueItemToHandler(handler)
        })
      })
    })

    handler.on('task-failed', (id, err) => {
      const item = _.find(_items, item => item._id.toString() === id)
      const task = item.getCurrentTask()

      item.currentTaskFailed(err)

      db.getDb().queue_items.save(item, () => {
        eventListenerManager.broadcast(`Failed_${item.operation}_${task.id}`, item)
        eventListenerManager.broadcast(`Failed_${task.id}`, task, err)
        eventListenerManager.broadcast(`Failed_${item.operation}_Task`, item, err)
        process.nextTick(() => {
          _queueItem(item)
          _queueItemToHandler(handler)
        })
      })
    })

    handler.on('task-delete', (id, err) => {
      const item = _.find(_items, item => item._id.toString() === id)
      const task = item.getCurrentTask()

      item.currentTaskFailed(err, 99999999)

      db.getDb().queue_items.remove({ _id: item._id }, function deleteOperation (errOnDelete) {
        if (errOnDelete) {
          return deleteOperation(id, err)
        }

        db.getDb().queue_item_history.save(item, () => {
          eventListenerManager.broadcast(`Deleted_${item.operation}_${task.id}`, item, err)
          eventListenerManager.broadcast(`Deleted_${task.id}`, task, err)
          eventListenerManager.broadcast(`Deleted_${item.operation}_Task`, item, err)
          eventListenerManager.broadcast(`Deleted_${item.operation}`, item, err)

          const index = _items.findIndex(x => x._id === item._id)
          if (index >= 0) {
            _items.splice(index, 1)
          } else {
            throw new Error('FFFF OMG')
          }

          process.nextTick(() => {
            _queueItemToHandler(handler)
          })
        })
      })
    })

    process.nextTick(() => {
      _queueItemsToHandler(handler)
    })

    logger.debug('Queue Item Handler client registered')
  })

  _this.on('queue-item-receiver-connected', receiver => {
    receiver.on('new-item', id => {
      if (process.askedToTerminate) {
        return
      }

      db.getDb().queue_items.findOne({ _id: id.toObjectID() }, (err, item) => {
        if (err) {
          return logger.error(`Receiver informed queue-service of new item, but item wasn't loaded: ${err}`)
        }

        if (!item) {
          return logger.info(`Receiver informed queue-service of new item, but item wasn't found: ${id}`)
        }

        item = new models.QueueItem(item)
        _items.push(item)

        eventListenerManager.broadcast(`Received_${item.operation}`, item)
        _queueItem(item)
      })
    })

    receiver.on('deregister', () => {
      logger.debug('Queue Item Receiver client deregistered')
    })

    receiver.on('disconnect', () => {
      logger.debug('Queue Item Receiver client disconnected')
    })

    logger.debug('Queue Item Receiver client registered')
  })

  const _queueItemsToHandler = handler => {
    if (process.askedToTerminate) {
      return
    }

    for (let i = 0; i < _items.length && !handler.maxLoadReached(); i++) {
      const item = _items[i]

      if (!!item.isAssigned() || !item.isAvailable()) {
        continue
      }

      const nextTask = item.getNextTask()
      if (!_.contains(handler.tasks, `${nextTask.id}@${nextTask.version}`)) {
        continue
      }

      _assignItemToHandler(item, handler)
    }
  }

  const _queueItemToHandler = handler => {
    if (process.askedToTerminate) {
      return
    }

    if (handler.maxLoadReached()) {
      return
    }

    const item = _.find(_items, item => {
      if (!!item.isAssigned() || !item.isAvailable()) {
        return false
      }

      const nextTask = item.getNextTask()
      return _.contains(handler.tasks, `${nextTask.id}@${nextTask.version}`)
    })

    if (!item) {
      return
    }

    _assignItemToHandler(item, handler)
  }

  const _queueItem = item => {
    if (process.askedToTerminate) {
      return
    }

    if (!item.isAvailable()) {
      return setTimeout(() => {
        _queueItem(item)
      }, item.durationTilNextAvailable())
    }

    if (!_handlers.length || item.isAssigned()) {
      return
    }

    const nextTask = item.getNextTask()
    let potentialHandlers = _.filter(_taskHandlers[`${nextTask.id}@${nextTask.version}`], handler => !handler.maxLoadReached())
    let selectedHandler

    if (!potentialHandlers || !potentialHandlers.length) {
      return
    }

    if (potentialHandlers.length > 1) {
      potentialHandlers = _.reduce(potentialHandlers, (data, handler) => {
        if (!data || !data.length) {
          data.push(handler)
          return data
        }

        if (handler.currentItems.length < data[0].currentItems.length) {
          return [handler]
        }

        if (handler.currentItems.length === data[0].currentItems.length) {
          data.push(handler)
        }

        return data
      }, [])

      selectedHandler = _.sample(potentialHandlers)
    } else {
      selectedHandler = potentialHandlers[0]
    }

    _assignItemToHandler(item, selectedHandler)
  }

  const _assignItemToHandler = (item, handler) => {
    item.assigned = true
    handler.queueItem(item)

    const task = item.beginNextTask()

    if (item.isFirstTask(task)) {
      eventListenerManager.broadcast(`Starting_${item.operation}`, item)
    }

    eventListenerManager.broadcast(`Starting_${item.operation}_Task`, item)
    eventListenerManager.broadcast(`Starting_${task.id}`, task)

    db.getDb().queue_items.save(item, (err, _item) => {
      if (err) {
        logger.warning('Error received but not catered for')
        console.error(err)
      }

      handler.processItem(item, err => {
        if (err) {
          item.currentTaskRejected()
          return db.getDb().queue_items.save(item, (err, _item) => {
            if (err) {
              logger.warning('Error received but not catered for')
              console.error(err)
            }

            process.nextTick(() => {
              _queueItem(item)
              _queueItemToHandler(handler)
            })
          })
        }

        if (item.isFirstTask(task)) {
          eventListenerManager.broadcast(`Started_${item.operation}`, item)
        }

        eventListenerManager.broadcast(`Started_${item.operation}_Task`, item)
        eventListenerManager.broadcast(`Started_${task.id}`, task)
      })
    })
  }

  _this.readyToTerminate = () => {
    const result = _.all(_items, item => !item.isAssigned())

    if (!result) {
      const assignedCount = _.filter(_items, item => item.isAssigned()).length
      logger.info(assignedCount)
    }

    return !!result
  }

  ;(function initialization () {
    const query = { 
      assigned: false,
      /*
      $or: [
        { nextAvailable: { $in: [ null, NaN] } },
        { nextAvailable: { $lt: +new Date() } },
        { nextAvailable: { $exists: false } }
      ]
      */
    }

    const getItems = (limit = 50, cb) => {
      cb = cb || function () {}

      return db.getDb()
        .queue_items
        .find(query)
        .sort({ dateCreated: -1 })
        .limit(limit, cb)
    }

     getItems(1000000, (err, items) => {
        if (err) {
          logger.warning('Error received but not catered for')
          console.error(err)
        }

        items = items.sort((a, b) => a.dateCreated - b.dateCreated)

        for (let i = 0; i < items.length; i++) {
          const item = new models.QueueItem(items[i])
          _items.push(item)
          _queueItem(item)
        }

        _this.emit('ready')

        setTimeout(() => {
          logger.debug('Starting check to see if any items were missed')

          getItems(30000, (err, items) => {
            if (err) {
              return
            }

            for (let i = items.length - 1; i >= 0; i--) {
              const alreadyContained = _.any(_items, item => item._id.toString() === items[i]._id.toString())

              if (alreadyContained) {
                items.splice(i, 1)
              }
            }

            if (!items || !items.length) {
              return
            }

            logger.debug(`Found ${items.length} items missing from local cache`)
            for (let i = 0; i < items.length; i++) {
              const item = new models.QueueItem(items[i])
              _items.push(item)
              _queueItem(item)
            }
          })
        }, 5000)
      })
  })()

  return _this
}

ItemManager.prototype = events.EventEmitter.prototype
module.exports = ItemManager
