module.exports = {
  CLIENT_TYPES: {
    QUEUE_ITEM_HANDLER: 1,
    QUEUE_ITEM_RECEIVER: 2,
    EVENT_LISTENER: 4
  }
}
