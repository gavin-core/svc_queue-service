const core = require('node-core')
const discovery = require('node-discovery')
const readline = require('readline')
const db = require('./middleware/db')
const Queue = require('./modules/queue')
const config = core.config

const Discovery = discovery.Discovery
const logger = core.logger
const servicePackage = discovery.servicePackage

let queue

/****************************************************************************************************************/

/**/
// if (!!core.config.safeMode || !!process.isChildProcess)
process.on('uncaughtException', err => {
  console.log('uncaught Exception:')
  console.log(err)
  console.log(err.stack)
})
/**/

/****************************************************************************************************************/
// SAFE EXITING

const _onExit = () => {
  logger.verbose(`${servicePackage.name} has been asked to terminate`)

  if (process.askedToTerminate) {
    return
  }

  process.askedToTerminate = true

  if (queue && queue.prepareForTerminate) {
    queue.prepareForTerminate()
  }

  _tryKill()
  setInterval(_tryKill, 1000)
  setTimeout(() => {
    logger.verbose(`${servicePackage.name} terminating by force, 20sec expired`)
    process.exit()
  }, 20000)
}

const _tryKill = () => {
  console.log('trying to kill')
  if (typeof queue !== 'function' || !queue.readyToTerminate || !queue.readyToTerminate()) {
    logger.verbose(`${servicePackage.name} terminating`)
    process.exit()
  }
}

process.on('SIGINT', _onExit)
  .on('SIGTERM', _onExit)
  .on('SAFE_KILL', _onExit)

if (process.platform === 'win32') {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.on('SIGINT', () => {
    process.emit('SIGTERM')
  })
}

/****************************************************************************************************************/

[
  function discover (next, data) {
    Discovery.discover(function (err, discInfo) {
      if (err) {
        return next(err)
      }

      logger.debug(`${servicePackage.name} starting on ${new Date()}`)
      data.discInfo = discInfo
      next()
    })
  },
  db.configureDbConnections,
  function initializeQueue (next, data) {
    queue = Queue()
    queue.on('ready', tcpInfo => {
      data.interfaces.tcp = tcpInfo
      next()
    })
  },
  function registerWithDiscovery (next, data) {
    Discovery.register({}, data.interfaces, next)
  }
].callInSequence(function (err) {
  if (err) {
    logger.error(err)
    return _onExit()
  }
}, {
  interfaces: {}
})

/****************************************************************************************************************/
